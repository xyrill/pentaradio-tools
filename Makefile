default: build/pentaradio-tools

GO_BUILDFLAGS = -mod vendor
GO_LDFLAGS    =

build/pentaradio-tools: FORCE
	go build $(GO_BUILDFLAGS) -ldflags '-s -w $(GO_LDFLAGS)' -o $@ .

DESTDIR =
PREFIX  = /usr

install: FORCE build/pentaradio-tools
	install -D -m 0755 build/pentaradio-tools "$(DESTDIR)$(PREFIX)/bin/pentaradio-tools"

vendor: FORCE
	go mod tidy -compat=$(shell awk '$$1 == "go" { print $$2 }' < go.mod)
	go mod vendor
	go mod verify

.PHONY: FORCE
