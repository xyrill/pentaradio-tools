/*******************************************************************************
* Copyright 2022 Stefan Majewsky <majewsky@gmx.net>
* SPDX-License-Identifier: GPL-3.0-only
* Refer to the file "LICENSE" for details.
*******************************************************************************/

package main

import (
	_ "embed"
	"errors"
	"fmt"
	"html/template"
	"os"
	"path/filepath"
	"strings"
	"time"
)

//go:embed news.xml.gotpl
var newsTemplateStr string

func Generate(scan ScanResult) {
	//the XML prelude gets mangled by html.Template, so we print it manually
	fmt.Println(`<?xml version="1.0" encoding="UTF-8"?>`)

	t, err := template.New("news.xml").Funcs(funcs).Parse(newsTemplateStr)
	must(err)
	must(t.Execute(os.Stdout, scan))
}

//Functions for the news.xml.gotpl template.
var funcs = map[string]interface{}{
	"now": func() string {
		return time.Now().Local().Format("2006-01-02T15:04:05")
	},

	"monthname": func(month int) string {
		return monthWords[month]
	},

	"filesize": func(file string) (int64, error) {
		fi, err := os.Stat(file)
		if err != nil {
			return 0, err
		}
		return fi.Size(), nil
	},

	"onlymp3": func(files []string) (string, error) {
		for _, file := range files {
			if strings.HasSuffix(file, ".mp3") {
				return file, nil
			}
		}
		return "", errors.New("no .mp3 audio file supplied")
	},

	"exceptmp3": func(files []string) (result []string) {
		for _, file := range files {
			if !strings.HasSuffix(file, ".mp3") {
				result = append(result, file)
			}
		}
		return
	},

	"mimetype": func(file string) (string, error) {
		switch filepath.Ext(file) {
		case ".mp3":
			return "audio/mpeg", nil
		case ".m4a":
			return "audio/mp4", nil
		case ".ogg":
			return "audio/ogg", nil
		case ".opus":
			return "audio/opus", nil
		default:
			return "", errors.New("do not know MIME type for " + filepath.Ext(file))
		}
	},
}
