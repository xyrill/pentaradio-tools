/*******************************************************************************
* Copyright 2023 Stefan Majewsky <majewsky@gmx.net>
* SPDX-License-Identifier: GPL-3.0-only
* Refer to the file "LICENSE" for details.
*******************************************************************************/

package main

import (
	"fmt"
	"time"
)

func PrintSchedule(count uint64) {
	now := time.Now().UTC()
	date := time.Date(
		now.Year(), now.Month(), now.Day(),
		12, 0, 0, 0,
		now.Location(),
	)

	for printed := uint64(0); printed < count; date = date.AddDate(0, 0, 1) {
		if date.Weekday() == time.Tuesday && date.Day() > 21 && date.Day() <= 28 {
			printDate(date)
			printed++
		}
	}
}

var germanMonths = []string{
	"Januar", "Februar", "März", "April", "Mai", "Juni",
	"Juli", "August", "September", "Oktober", "November", "Dezember",
}

func printDate(t time.Time) {
	fmt.Printf(
		"%s = %d. %s %d\n",
		t.Format(time.DateOnly),
		t.Day(),
		germanMonths[t.Month()-1],
		t.Year(),
	)
}
