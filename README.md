# pentaradio-tools

A tool to automate parts of the publishing process of Pentaradio episodes.

## Installation

Compile and install with `make` and `make install`. A Go compiler is required.

For the `pentaradio-tools upload` subcommand, `ftp.c3d2.de` must be configured in the SSH client config, for example:

```
$ cat $HOME/.ssh/config
...
Host ftp.c3d2.de
  User ftpupload
  Port 2022
  IdentityFile /home/username/.ssh/id_ed25519
  IdentitiesOnly yes
...
```

## Usage

There are two subcommands, both without any additional arguments or options:

* `pentaradio-tools upload` uploads all relevant files (audios, chapter marks file, shownotes file) to `ftp.c3d2.de`.
* `pentaradio-tools generate` generates an XML file containing a news entry for [c3d2-web](/c3d2/c3d2-web).

Both subcommands expect the following files in the working directory:

```
pentaradio-YYYY-MM-DD.m4a
pentaradio-YYYY-MM-DD.mp3
pentaradio-YYYY-MM-DD.ogg
pentaradio-YYYY-MM-DD.opus
chapter-pentaradio-YYYY-MM-DD.dat
shownotes-pentaradio-YYYY-MM-DD.txt
```

The date must be identical on all files.

The chapter marks file must contain the same marks that have been given to Auphonic. The usual process is to write the
chapter marks file during editing and then give it to Auphonic's file importer.

The shownotes file must contain the contents of the shownotes pad. It is technically a Markdown file, but for historical
reasons we are using the `.txt` extension. The shownotes must be in the following format:

```
# Pentaradio vom DD. Mmm YYYY
# Titel: "<title>"

Mit <author-names>.

<short-description>

## News
...
```

The title, author names, and short description are used for the c3d2-web news entry.
