/*******************************************************************************
* Copyright 2022 Stefan Majewsky <majewsky@gmx.net>
* SPDX-License-Identifier: GPL-3.0-only
* Refer to the file "LICENSE" for details.
*******************************************************************************/

package main

import (
	"os"
	"os/exec"
	"strings"
)

func Upload(scan ScanResult) {
	sftpCommands := []string{
		"cd pentaradio/shownotes",
		"put " + scan.ShowNotesFile(),
		"cd ../chaptermarks",
		"put " + scan.ChapterMarksFile(),
		"cd ..",
	}
	for _, fileName := range scan.AudioFileNames {
		sftpCommands = append(sftpCommands, "put "+fileName)
	}

	cmd := exec.Command("sftp", "-b", "-", "ftp.c3d2.de")
	cmd.Stdin = strings.NewReader(strings.Join(sftpCommands, "\n") + "\n")
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	must(cmd.Run())
}
