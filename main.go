/*******************************************************************************
* Copyright 2022 Stefan Majewsky <majewsky@gmx.net>
* SPDX-License-Identifier: GPL-3.0-only
* Refer to the file "LICENSE" for details.
*******************************************************************************/

package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"
)

func main() {
	if len(os.Args) < 2 {
		usage(1)
	}
	subcommand, args := os.Args[1], os.Args[2:]

	switch subcommand {
	case "upload":
		if len(args) != 0 {
			usage(1)
		}
		Upload(Scan())
	case "generate":
		if len(args) != 0 {
			usage(1)
		}
		Generate(Scan())
	case "schedule":
		if len(args) != 1 {
			usage(1)
		}
		count, err := strconv.ParseUint(args[0], 10, 32)
		must(err)
		PrintSchedule(count)
	case "help", "--help":
		if len(args) != 0 {
			usage(1)
		}
		usage(0)
	default:
		usage(1)
	}
}

func must(err error) {
	if err != nil {
		fail(err.Error())
	}
}

func fail(msg string, args ...interface{}) {
	if len(args) > 0 {
		msg = fmt.Sprintf(msg, args...)
	}
	fmt.Fprintln(os.Stderr, "ERROR: "+msg)
	os.Exit(1)
}

func usage(exitCode int) {
	fmt.Fprintf(os.Stderr,
		"Usage:\n\n"+
			"  %[1]s upload       - Upload all relevant files for one episode to <sftp://ftp.c3d2.de>.\n"+
			"  %[1]s generate     - Generate the c3d2-web news entry XML file for one episode.\n"+
			"  %[1]s schedule <N> - Print the next N airdates.\n\n"+
			"For `upload` and `generate`, all relevant files (audios, chapter marks file, shownotes Markdown file) must be in the working directory.\n",
		filepath.Base(os.Args[0]),
	)
	os.Exit(exitCode)
}
